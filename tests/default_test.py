from test_runner_gitea._version import __version__

def test_version():
    assert __version__ == "0.0.1"